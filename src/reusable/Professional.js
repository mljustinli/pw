import React, { Component } from "react";

const images = require.context("../images/professional-img", true);

class Professional extends Component {
    getInformation() {
        return [
            {
                company: "Worldpay",
                title: "Software Engineer Intern",
                bullets: [
                    "Developed a standalone Java application to process and visualize thousands of payment entries by compiling it into a database and sending it to a graphing program",
                    "Replaced Adobe Flash visualizations on 8 customer-facing webpages by implementing JavaScript visualization libraries",
                    "Reduced compilation time by 20% by creating bash scripts to automate tasks"
                ],
                img: "worldpayFeature.png"
            },
            {
                company: "KTByte Computer Camp",
                title: "Teacher and Web Developer",
                bullets: [
                    "Designed self-guided lesson plans",
                    "Taught students age 4 to 12 computer science concepts through Processing and Java Blocks",
                    "Created example projects in Processing and Java Blocks",
                    "Enhanced the design of KTByte's press page"
                ],
                img: "ktbyteFeature.png"
            },
            {
                company: "Harvard Smithsonian (Minor Planet Center)",
                title: "Web Developer",
                bullets: [
                    "Revitalized the Minor Planet Center website and developed a user-friendly website to allow users to access the MPC database",
                    "Used PHP to interface with the MPC database"
                ],
                img: "mpcFeature.jpg"
            }
        ];
    }

    render() {
        const proSectionList = this.getInformation().map((item, index) => (
            <ProfessionalSection
                company={item.company}
                title={item.title}
                bullets={item.bullets}
                img={item.img}
                left={index % 2 === 0}
            />
        ));

        return <div className="professional-wrapper">{proSectionList}</div>;
    }
}

class ProfessionalSection extends Component {
    getInfo() {
        const bulletList = this.props.bullets.map(item => (
            <DescriptionRow text={item} />
        ));

        return (
            <div className="info-wrapper">
                <div className="pro-top-wrapper">
                    <div className="align-padding align-padding-width"></div>
                    <div className="title-wrapper">
                        <div className="company-name">{this.props.company}</div>
                        <div className="job-title">{this.props.title}</div>
                    </div>
                </div>
                <div className="pro-bottom-wrapper">{bulletList}</div>
            </div>
        );
    }

    getImage() {
        let loadedImg = images("./" + this.props.img);

        var bgStyle = {
            backgroundImage: "url(" + loadedImg + ")"
        };

        return (
            <div className="image-wrapper" style={bgStyle}>
                <svg
                    className={
                        this.props.left
                            ? "trapezoid"
                            : "trapezoid trapezoid-right"
                    }
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 100 100"
                    preserveAspectRatio="none"
                >
                    <polygon
                        fill="white"
                        points={
                            this.props.left
                                ? "0,0 0,100 30,0"
                                : "70,100 100,0 100,100"
                        }
                    />
                </svg>
                <div className="pro-image-darkener"></div>
            </div>
        );
    }

    render() {
        return (
            <div className="professional-section">
                {!this.props.left && this.getImage()}
                {this.getInfo()}
                {this.props.left && this.getImage()}
            </div>
        );
    }
}

class DescriptionRow extends Component {
    render() {
        return (
            <div className="description-row">
                <div className="svg-bullet-wrapper align-padding-width">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-globe"
                    >
                        <circle cx="12" cy="12" r="10"></circle>
                        <line x1="2" y1="12" x2="22" y2="12"></line>
                        <path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path>
                    </svg>
                </div>
                <div className="description-text">{this.props.text}</div>
            </div>
        );
    }
}

export default Professional;
