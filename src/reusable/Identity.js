import React, { Component } from "react";

// React Reveal
import Fade from "react-reveal/Fade";

class Identity extends Component {
    getIdentities() {
        return [
            {
                title: "Software Engineer",
                description:
                    "I believe in maintainable, efficient, and quality code."
            },
            {
                title: "Web Developer",
                description:
                    "I make websites for fun, and it would be great to make them professionally."
            },
            {
                title: "Game Developer",
                description:
                    "I make games, from creating 2D and 3D assets, programming game objects, and deploying finished products on Google Play or itch.io."
            },
            {
                title: "Leader",
                description:
                    "I oversee the electronics subteam of Georgia Tech’s Hyperloop team."
            }
        ];
    }

    render() {
        const idData = this.getIdentities();
        const idHTML = idData.map(item => (
            <IdentitySection
                title={item.title}
                description={item.description}
                key={item.title}
            ></IdentitySection>
        ));

        return (
            <div id="identity-wrapper">
                <div>{idHTML}</div>
            </div>
        );
    }
}

class IdentitySection extends Component {
    render() {
        return (
            <div className="identity-section">
                <Fade bottom big cascade>
                    <div className="identity-whole-title">
                        <span>I am a</span>{" "}
                        <span className="identity-title">
                            {this.props.title}
                        </span>
                    </div>
                    <div className="identity-description">
                        <span>{this.props.description}</span>
                    </div>
                </Fade>
            </div>
        );
    }
}

export default Identity;
