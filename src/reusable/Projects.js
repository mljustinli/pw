import React, { Component } from "react";
import Fade from "react-reveal/Fade";

const images = require.context("../images/projects-img", true);

// First two are featured!
var information = [
    {
        title: "Waste!",
        description:
            "Currently developing a leftover and grocery management web application. The app will send users reminders about leftovers that they have and also remind users to use certain ingredients before they start to cook.",
        projectLink: "https://gitlab.com/mljustinli/waste-not",
        gitLink: "https://gitlab.com/mljustinli/waste-not",
        img: "wasteNot2.png"
    },
    {
        title: "Swipe Brick Breaker Neural Net",
        description:
            "Trained neural nets to learn to play Swipe Brick Breaker using NEAT (NeuralEvolution of Augmenting Topologies). The top score is 111.",
        projectLink: "https://mljustinli.gitlab.io/swipe-block-neural-net/",
        gitLink: "https://gitlab.com/mljustinli/swipe-block-neural-net",
        img: "swipeNeuralNet.png"
    },
    {
        title: "Dashing Dino",
        description:
            "Experimented with 3D design and created an endless obstacle game.",
        projectLink:
            "https://play.google.com/store/apps/details?id=com.JuiceTin.DinoGame",
        gitLink: "https://gitlab.com/mljustinli/dashing-dino-code-samples",
        img: "dashingDino.png"
    },
    {
        title: "Tower of Babel",
        description:
            'Designed a 2D exploration game based on the short story, "Tower of Babylon," by Ted Chiang.',
        projectLink:
            "https://play.google.com/store/apps/details?id=com.JuiceTin.DinoGame",
        gitLink: "",
        img: "babel.png"
    },
    {
        title: "Minor Planet Checker",
        description:
            "Revitalized the Minor Planet Center website and developed a user-friendly website to allow users to access the MPC database.",
        projectLink: "",
        gitLink: "",
        img: "asteroid.jpg"
    },
    {
        title: "MetronoMe",
        description:
            "Created a customizable metronome during LexHack 2016. My friend and I won 2nd place!",
        projectLink: "https://devpost.com/software/subdivide",
        gitLink: "https://github.com/choiben314/metronome",
        img: "androidLogo.png"
    }
    // {
    //     title: "",
    //     description: "",
    //     projectLink: "",
    //     gitLink: "",
    //     img: ""
    // }
];

class Projects extends Component {
    constructor(props) {
        super(props);

        this.state = { firstRow: 18, otherRows: 4 };
    }

    render() {
        var projectList = information.map((item, index) => (
            <div
                className={
                    "projects-grid-elem" +
                    " " +
                    (index < this.state.firstRow ? "" : "projects-grid-elem-4")
                }
                style={{
                    backgroundImage:
                        "url(" + images("./" + information[index].img) + ")"
                }}
            >
                <GridElement
                    feature={index < this.state.firstRow}
                    index={index}
                />
            </div>
        ));

        return <div id="projects-grid">{projectList}</div>;
    }
}

class GridElement extends Component {
    constructor(props) {
        super(props);

        this.state = { hovered: false };

        this.toggleHover = this.toggleHover.bind(this);
    }

    toggleHover() {
        this.setState({ hovered: !this.state.hovered });
    }

    render() {
        return this.props.feature ? (
            <div
                className="grid-elem-inner"
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}
            >
                <div
                    className="project-img-darkener"
                    style={
                        this.state.hovered
                            ? {
                                  backgroundColor: "rgba(0, 0, 0, 0.6)"
                              }
                            : { backgroundColor: "rgba(0, 0, 0, 0.2)" }
                    }
                ></div>
                <Fade bottom when={this.state.hovered}>
                    <div className="title-desc-wrapper">
                        <div className="project-title">
                            {information[this.props.index].title}
                        </div>
                        <div className="project-description">
                            {information[this.props.index].description}
                        </div>
                    </div>
                </Fade>
                <LinkGroup
                    index={this.props.index}
                    hovered={this.state.hovered}
                />
            </div>
        ) : (
            <div
                className="grid-elem-inner"
                onMouseEnter={this.toggleHover}
                onMouseLeave={this.toggleHover}
            >
                <div
                    className="project-img-darkener"
                    style={
                        this.state.hovered
                            ? {
                                  backgroundColor: "rgba(0, 0, 0, 0.6)"
                              }
                            : { backgroundColor: "rgba(0, 0, 0, 0.2)" }
                    }
                ></div>
                <LinkGroup
                    index={this.props.index}
                    hovered={this.state.hovered}
                />
            </div>
        );
    }
}

class LinkGroup extends Component {
    render() {
        return (
            <div className="links-wrapper">
                <a
                    href={information[this.props.index].projectLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div className="links-icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            className="feather feather-link-2"
                        >
                            <path d="M15 7h3a5 5 0 0 1 5 5 5 5 0 0 1-5 5h-3m-6 0H6a5 5 0 0 1-5-5 5 5 0 0 1 5-5h3"></path>
                            <line x1="8" y1="12" x2="16" y2="12"></line>
                        </svg>
                    </div>
                </a>
                <a
                    href={information[this.props.index].gitLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div className="links-icon">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            className="feather feather-git-pull-request"
                        >
                            <circle cx="18" cy="18" r="3"></circle>
                            <circle cx="6" cy="6" r="3"></circle>
                            <path d="M13 6h3a2 2 0 0 1 2 2v7"></path>
                            <line x1="6" y1="9" x2="6" y2="21"></line>
                        </svg>
                    </div>
                </a>
            </div>
        );
    }
}

export default Projects;
