import React, { Component } from "react";

// Components

import bannerPhoto from "../images/banner-photo.jpg";

// React Reveal
import Fade from "react-reveal/Fade";

class Banner extends Component {
    render() {
        return (
            <div id="banner">
                <img id="banner-photo" src={bannerPhoto} alt="banner" />
                <div id="banner-darkener"></div>
                <Fade bottom>
                    <div id="banner-text">Justin Li</div>
                </Fade>
            </div>
        );
    }
}

export default Banner;
