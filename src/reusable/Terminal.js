import React, { Component } from "react";

class Terminal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            terminalText: "",
            charIndex: 0,
            lineCount: 0,
            showCursor: true
        };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.doWait(), 1000);
        // window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);

        // window.removeEventListener("scroll", this.handleScroll);
    }

    setUpCursorBlink() {
        this.blinkID = setInterval(() => this.doCursorBlink(), 500);
    }

    doCursorBlink() {
        this.setState({ showCursor: !this.state.showCursor });
    }

    clearCursorBlink() {
        clearInterval(this.blinkID);
    }

    doWait() {
        clearInterval(this.timerID);
        this.timerID = setInterval(() => this.doType(), 100);
    }

    doType() {
        this.setState({
            terminalText:
                this.state.terminalText +
                this.props.children.charAt(this.state.charIndex)
        });

        this.setState({ charIndex: this.state.charIndex + 1 });

        if (this.state.charIndex >= this.props.children.length) {
            // this.setState({ terminalText: "herro" });
            clearInterval(this.timerID);
            this.timerID = setInterval(() => this.doAddLines(), 100);
        }
    }

    doAddLines() {
        this.setState({ lineCount: this.state.lineCount + 1 });

        if (this.state.lineCount >= this.props.extraLines) {
            clearInterval(this.timerID);
            this.setUpCursorBlink();
        }
    }

    render() {
        var lines = [];
        for (var i = 0; i < this.state.lineCount; i++) {
            lines.push(
                <span className="terminal-line" key={i}>
                    <br />>
                </span>
            );
        }

        const date = new Date();

        const terminalPre =
            window.innerWidth > 768
                ? "[" +
                  (date.getMonth() + 1) +
                  "-" +
                  date.getDate() +
                  "-" +
                  date.getFullYear() +
                  "] ~$ "
                : "$ ";

        return (
            <div className="terminal-wrapper terminal-overflow">
                <div id={this.props.id} className="terminal">
                    <div className="code-font">
                        {terminalPre}
                        <span className="terminal-user-input">
                            {this.state.terminalText}
                        </span>
                        {lines}
                        {this.state.showCursor && (
                            <span className="terminal-cursor">_</span>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default Terminal;
