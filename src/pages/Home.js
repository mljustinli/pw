import React, { Component } from "react";

// Components
import Banner from "../reusable/Banner";
import Terminal from "../reusable/Terminal";
import Identity from "../reusable/Identity";
import Professional from "../reusable/Professional";
import Projects from "../reusable/Projects";

class Home extends Component {
    render() {
        return (
            <div id="page-wrapper">
                <Banner />

                <Terminal id="intro-terminal" extraLines={1}>
                    ./introduction.sh
                </Terminal>

                <div className="dither-gradient down"></div>

                <Identity />

                <Terminal id="professional-terminal" extraLines={1}>
                    ./professional.sh
                </Terminal>

                <Professional />

                <Terminal id="project-terminal" extraLines={1}>
                    open projects.rtf
                </Terminal>

                <Projects />

                <Terminal id="activities-terminal" extraLines={1}>
                    more activities.txt
                </Terminal>
            </div>
        );
    }
}

export default Home;
